import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix

def normalised_cmx(y, y_predicted, show=True, return_mx=False, zero_diag=True):
    """
    return a normalised confusion matrix
    """

    conf_mx_labels = np.sort(np.unique(y))
    conf_mx = confusion_matrix(y, y_predicted, labels=conf_mx_labels)

    if zero_diag:
        np.fill_diagonal(conf_mx, 0)

    norm_conf_mx = conf_mx / conf_mx.sum(axis=1, keepdims=True)
    print(conf_mx_labels)
    if show:
        plt.matshow(norm_conf_mx)
    if return_mx:
        return norm_conf_mx