**obsolete**

This repo contains exploration for ocr and equipment recognision at
infrabel. 

The scikit-learn pipeline tutorial was based on this. The code for the
tutorial has moved to:

https://gitlab.com/Kapernikov/Intern/sklearn-pipeline-tutorial
