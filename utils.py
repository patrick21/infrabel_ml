import cv2
import matplotlib.pyplot as plt
import numpy as np
import os


PATH = r'C:\cygwin64\home\EXK478\computervision\equipment-photos'
PLP_PATH = os.path.join(PATH, 'plp')
image_paths = [os.path.join(PLP_PATH, plp) for plp in os.listdir(PLP_PATH)]


def bgr2rgb(img):
    return img[:,:,::-1]


def imshow(img):
    b, g, r = cv2.split(img)
    return cv2.merge((r, g, b))


def rg_chrom(img):
    '''assumes bgr im and makes rg chromic'''
    
    img = np.float32(img)
    sumRGB = np.sum(img, axis=2)
    sumRGB = np.where(sumRGB, sumRGB, 1)
    return img/sumRGB[:,:,np.newaxis]
    

def show_channels(img, noreturn=True):
    '''load a colour image and return the colour, greyscale and r,g,b channels'''
    
    B, G, R = cv2.split(img)
    grey = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    RGB = cv2.merge((R, G, B))
    r, g, b = rg_chrom(img)
    rgb = cv2.merge((R, G, B))
    HSV = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    H, S, V = cv2.split(HSV)
    
    fig, ax = plt.subplots(3, 4)
    fig.set_size_inches(10,10)
    fig.tight_layout()
    ax[0, 0].set_title('RGB')
    ax[0, 0].imshow(RGB)
#     ax[0, 1].set_title('greyscale')
#     ax[0, 1].imshow(grey , cmap='gray')
    ax[0, 1].set_title('Red')
    ax[0, 1].imshow(R, cmap='gray')
    ax[0, 2].set_title('Green')
    ax[0, 2].imshow(G, cmap='gray')
    ax[0, 3].set_title('Blue')
    ax[0, 3].imshow(B, cmap='gray')
    
    ax[1, 0].set_title('rgb')
    ax[1, 0].imshow(rgb)
    ax[1, 1].set_title('red')
    ax[1, 1].imshow(r, cmap='gray')
    ax[1, 2].set_title('green')
    ax[1, 2].imshow(g, cmap='gray')
    ax[1, 3].set_title('blue')
    ax[1, 3].imshow(b, cmap='gray')
    
    ax[2, 0].set_title('HSV')
    ax[2, 0].imshow(HSV)
    ax[2, 1].set_title('H')
    ax[2, 1].imshow(H, cmap='gray')
    ax[2, 2].set_title('S')
    ax[2, 2].imshow(S, cmap='gray')
    ax[2, 3].set_title('V')
    ax[2, 3].imshow(V, cmap='gray')


    for a in ax.flatten():
        a.set_xticks([])
        a.set_yticks([])
        a.set_xticklabels([])
        a.set_yticklabels([])
    
    if not noreturn:
        return fig, ax
    

def thresh_bradley_roth(img, s=None, t=None):
    '''
    https://stackoverflow.com/questions/33091755/bradley-roth-adaptive-thresholding-algorithm-how-do-i-get-better-performance

    The function takes in three parameters: the grayscale image image, the size
    of the window s and the threshold t. This threshold is different than what
    you have as this is following the paper exactly. The threshold t is a per-
    centage of the total summed area of each pixel window. If the summed area is
    less than this threshold, then the output should be a black pixel - else it's
    a white pixel. The defaults for s and t are the number of columns divided by
    8 and rounded, and 15% respectively

    '''

    # convert to greyscale if colour
    if len(img.shape) == 3:
        raise ValueError('input image should be in greyscale')

    # Default window size is round(cols/8)
    if s is None:
        s = np.int16(np.round(img.shape[1] / 8))

    # Default threshold is 15% of the total
    # area in the window
    if t is None:
        t = 15.0

    # Compute integral image
    intImage = np.cumsum(np.cumsum(img, axis=1), axis=0)

    # Define grid of points
    (rows, cols) = img.shape[:2]
    (X, Y) = np.meshgrid(np.arange(cols, dtype=np.int16), np.arange(rows, dtype=np.int16))

    # Make into 1D grid of coordinates for easier access
    X = X.ravel()
    Y = Y.ravel()

    # Ensure s is even so that we are able to index into the image
    # properly
    s = s + np.mod(s, 2)

    # Access the four corners of each neighbourhood
    s2 = np.floor_divide(s, 2)
    x1 = X - s2
    x2 = X + s2
    y1 = Y - s2
    y2 = Y + s2

    # Ensure no coordinates are out of bounds
    x1[x1 < 0] = 0
    x2[x2 >= cols] = cols - 1
    y1[y1 < 0] = 0
    y2[y2 >= rows] = rows - 1

    # Count how many pixels are in each neighbourhood
    count = (x2 - x1) * (y2 - y1)

    # Compute the row and column coordinates to access
    # each corner of the neighbourhood for the integral image
    f1_x = x2
    f1_y = y2
    f2_x = x2
    f2_y = y1 - 1
    f2_y[f2_y < 0] = 0
    f3_x = x1 - 1
    f3_x[f3_x < 0] = 0
    f3_y = y2
    f4_x = f3_x
    f4_y = f2_y

    # Compute areas of each window
    sums = intImage[f1_y, f1_x] - intImage[f2_y, f2_x] - intImage[f3_y, f3_x] + intImage[f4_y, f4_x]

    # Compute thresholded image and reshape into a 2D grid
    out = np.ones(rows * cols, dtype=np.bool)
    out[img.ravel() * count <= sums * (100.0 - t) / 100.0] = False

    # Also convert back to uint8
    out = 255 * np.reshape(out, (rows, cols)).astype(np.uint8)

    return out
