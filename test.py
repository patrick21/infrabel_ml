import numpy as np
from sklearn.externals import joblib
from sklearn.model_selection import train_test_split
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler
from sklearn import svm
from sklearn.model_selection import GridSearchCV

from infrabel_ml.transformers import HogTransformer, RGB2GrayTranformer


if __name__ == '__main__':

    data = joblib.load('equipment_photos_102x136px.pkl')

    X = np.array(data['data'])
    y = np.array(data['label'])
    fn = np.array(data['filename'])

    X_train, X_test, y_train, y_test = train_test_split(
        X,
        y,
        test_size=0.2,
        shuffle=True,
        random_state=42,
    )
    HOG_pipeline = Pipeline([
        ('grayify', RGB2GrayTranformer()),
        ('hogify', HogTransformer()),
        ('scalify', StandardScaler()),
        ('classify', svm.SVC(kernel='linear'))
    ])

    param_grid = [
        {'hogify__orientations': [7, 9, 11, 13],
        'hogify__cells_per_block': [(3,3), (5,5), (7,7)],
        'hogify__pixels_per_cell': [(8,8), (12,12), (14,14)]}
    ]

    grid_search = GridSearchCV(HOG_pipeline,
                               param_grid,
                               cv=3,
                               n_jobs=3,
                               scoring='accuracy',
                               verbose=2)

    cvres = grid_search.fit(X_train, y_train)
    joblib.dump(cvres, 'cvres.pkl')
