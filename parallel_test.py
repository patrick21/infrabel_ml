from sklearn.externals.joblib import Parallel, delayed
import numpy as np
import sys
import time

def foo(n):
    return Parallel(n_jobs=4)(delayed(np.sqrt)(i ** 2) for i in range(n))

if __name__ == '__main__':
    t0 = time.time()
    foo(int(sys.argv[1]))
    print(time.time() - t0)

    t0 = time.time()
    [np.sqrt(i**2) for i in range(int(sys.argv[1]))]
    print(time.time() - t0)
